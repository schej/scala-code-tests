# CountSquares

This project is a coding challenge in Scala for Schej.ai. 

## The task

This challenge is to write a function which, given two input integers a and b, finds the number of perfect squares between those numbers. 

A perfect square is a number made by squaring a whole number, e.g. 1, 4, 9, 16. 0 is also considered a perfect square.

For example, if a = 5 and b = 26, the number of perfect squares is 3: 9, 16 and 25.

This should be inclusive. For example, if a = 4 and b = 5, the function should return 1 because a is a perfect square. 

In CountSquares.scala, the skeleton of the app is provided, which takes input from the user and calls a countSquares function. Your task is to implement this function. 

You do not need to consider any type checking of the user input - assume that the user enters integers for both numbers. However, you should consider that all of the following are possible:

- a and b are equal
- a and / or b could be negative
- b could be less than a. In this case, the number of perfect squares between a and b should still be counted.

In /src/test/scala/ai.schej.CountSquares.scala, there are some unit tests with various test case. You don't necessarily need to run the tests, but they can help you to validate your code.

## Running your code

You'll need Scala and SBT running on your computer to run the code. This can be either through an IDE like IntelliJ, or by downloading it from https://www.scala-lang.org/download/. 

If you're using an IDE, you'll need to follow its instructions to run a Scala app. If you've installed SBT, on the command line, navigate to the CountSquares directory and type "sbt run".

The test directory contains a number of unit tests which can help you confirm your code is working. You can run these using the command "sbt test".

## What are we looking for?

We'll be looking for the following features as positives:

- Does the code generate the right response?
- How well does the code handle edge cases?
- How well does the code perform, e.g. for very large differences between a and b?
- How readable is the code?  Could someone else looking at the code understand it easily?
