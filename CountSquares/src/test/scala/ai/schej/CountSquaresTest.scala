package ai.schej

import org.scalatest._
import org.scalatest.flatspec._
import org.scalatest.matchers._

class CountSquaresTest extends AnyFlatSpec with should.Matchers {
  "CountSquares" should "return the correct value when a < b and both are positive" in {
    CountSquares.countSquares(0, 26) should be (6)
  }

  it should "return the correct value when a > b and both are positive" in {
    CountSquares.countSquares(26, 0) should be (6)
  }

  it should "return the correct value when a < b and a is negative" in {
    CountSquares.countSquares(-1000, 26) should be (6)
  }

  it should "return 0 when a and b are both negative" in {
    CountSquares.countSquares(-1000, -1) should be (0)
  }

  it should "return 0 when a and b are equal and not square" in {
    CountSquares.countSquares(3, 3) should be (0)
  }

  it should "return 1 when a and b are equal and square" in {
    CountSquares.countSquares(4, 4) should be (1)
  }

  it should "consider 0 to be a perfect square" in {
    CountSquares.countSquares(0, 0) should be (1)
  }

}
