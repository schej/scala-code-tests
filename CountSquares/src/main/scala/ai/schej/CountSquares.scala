package ai.schej

import scala.io.StdIn

/**
 * Scala coding test for Schej.ai.
 *
 * Please see README.md for instructions on how to complete the test.
 */
object CountSquares extends App {
  run()

  def run(): Unit = {
    println("Please enter the first number:")
    val a = StdIn.readInt()

    println("Please enter the second number:")
    val b = StdIn.readInt()

    val numberOfSquares = countSquares(a, b)
    println(s"The number of squares between $a and $b is $numberOfSquares")
  }

  /**
   * Given a pair of integers, count the number of perfect squares which exist between them.
   *
   * @param a The first integer
   * @param b The second integer
   *
   * @return The number of perfect squares which exist between a and b, including those numbers.
   */
  def countSquares(a: Int, b: Int): Int = {
    1 // Replace this with your implementation
  }
}